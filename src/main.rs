#![feature(test)]

extern crate test;

use test::{black_box, Bencher};

use std::sync::atomic::{AtomicBool, AtomicUsize};
use std::sync::Arc;
use std::thread::spawn;

fn main() {}

const TEST_SIZE: usize = 1000_000;

#[bench]
fn test1(bencher: &mut Bencher) {
    bencher.iter(|| {
        let stop = Arc::new(AtomicBool::new(false));

        let mut joins = vec![];


        for i in 0..1 {
            joins.push(spawn(|| {
                let a = 8450348523409823;
                let b = 234;

                for _ in 0..TEST_SIZE {
                    black_box(black_box(a) / black_box(b));
                }
            }));
        }

        joins.into_iter().for_each(|t| {
            t.join().is_ok();
        });
    });
}
#[bench]
fn test2(bencher: &mut Bencher) {
    bencher.iter(|| {
        let stop = Arc::new(AtomicBool::new(false));

        let mut joins = vec![];


        for i in 0..2 {
            joins.push(spawn(|| {
                let a = 8450348523409823;
                let b = 234;

                for _ in 0..TEST_SIZE {
                    black_box(black_box(a) / black_box(b));
                }
            }));
        }

        joins.into_iter().for_each(|t| {
            t.join().is_ok();
        });
    });
}
#[bench]
fn test3(bencher: &mut Bencher) {
    bencher.iter(|| {
        let stop = Arc::new(AtomicBool::new(false));

        let mut joins = vec![];


        for i in 0..3 {
            joins.push(spawn(|| {
                let a = 8450348523409823;
                let b = 234;

                for _ in 0..TEST_SIZE {
                    black_box(black_box(a) / black_box(b));
                }
            }));
        }

        joins.into_iter().for_each(|t| {
            t.join().is_ok();
        });
    });
}
#[bench]
fn test4(bencher: &mut Bencher) {
    bencher.iter(|| {
        let stop = Arc::new(AtomicBool::new(false));

        let mut joins = vec![];


        for i in 0..4 {
            joins.push(spawn(|| {
                let a = 8450348523409823;
                let b = 234;

                for _ in 0..TEST_SIZE {
                    black_box(black_box(a) / black_box(b));
                }
            }));
        }

        joins.into_iter().for_each(|t| {
            t.join().is_ok();
        });
    });
}
