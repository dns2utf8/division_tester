# Division Tester

## Some Test Results:

Run on a Intel i7-6600U

### Single Thread
```
taskset -c 0 cargo +nightly bench

test test1 ... bench:   3,505,924 ns/iter (+/- 76,587)
test test2 ... bench:   7,054,193 ns/iter (+/- 225,908)
test test3 ... bench:  10,605,628 ns/iter (+/- 288,635)
test test4 ... bench:  14,161,795 ns/iter (+/- 337,858)
```

## Single with HyperThread
```
taskset -c 0,2 cargo +nightly bench

test test1 ... bench:   3,526,366 ns/iter (+/- 134,882)
test test2 ... bench:   6,419,750 ns/iter (+/- 150,406)
test test3 ... bench:   9,931,937 ns/iter (+/- 477,771)
test test4 ... bench:  12,851,376 ns/iter (+/- 217,413)
```

## Two Cores
```
taskset -c 0-1 cargo +nightly watch -x bench

test test1 ... bench:   3,638,333 ns/iter (+/- 199,325)
test test2 ... bench:   3,986,634 ns/iter (+/- 3,207,906)
test test3 ... bench:   7,252,515 ns/iter (+/- 367,326)
test test4 ... bench:   7,453,967 ns/iter (+/- 635,212)
```

## Full System
```
cargo +nightly bench

test test1 ... bench:   3,830,973 ns/iter (+/- 395,008)
test test2 ... bench:   3,947,137 ns/iter (+/- 392,900)
test test3 ... bench:   6,798,071 ns/iter (+/- 173,311)
test test4 ... bench:   7,115,727 ns/iter (+/- 296,174)
```
